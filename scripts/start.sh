#! /usr/bin/env sh

./scripts/wait-for.sh postgres:5432 -- echo "postgres is up"
./scripts/wait-for.sh rabbitmq:5672 -- echo "rabbitmq is up"

python src/photo_processor.py &

export FLASK_APP=src/api.py
export FLASK_RUN_HOST=0.0.0.0
export FLASK_RUN_PORT=3000
flask run 

