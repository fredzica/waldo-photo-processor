## photo-processor exercise

### Description

The aim of the exercise is to orchestrate the generation of thumbnails for a specific set of photos.

### Structure

The application consists of a Postgres database, a RabbitMQ service, a Flask API and a queue consumer.

- The Postgres stores image metadata and the status of the thumbnail generation process
- The queue service has a queue where each item is a task to generate a thumbnail from a larger image
- The Flask API enables listing of the image-related data and triggering of the thumbnail generation process
- The consumer is a Python script responsible for retrieving items from the queue and generating their respective thumbnails

The thumbnails are generated in the `/waldo-app-thumbs` folder in the container that is executing the consumer.

### Installation

Prerequisites:  
- Docker  
- Ability to run `make`.

App is bundling Postgres and RabbitMQ instances via Docker, so please stop any local related services to avoid port conflicts. Otherwise you can amend the default port mappings on the docker-compose file.

Start the app:
- `make start`

Create or reset the db schema after booting the app:  
- `make db-schema`

Postgres PSQL can be accessed via:
- `make psql`

RabbitMQ management console can be accessed at:  
`http://localhost:15672/`  

Web app can be accessed at:  
`http://localhost:3000/`  

#### Added make targets

Updates the container running the main app with modified source files
- `make update-app`

Runs the unit tests (depends on `pipenv`)
- `make test`

### Gitlab CI

The Gitlab CI is activated for this project. Its definition can be seen on the `.gitlab-ci.yml` file. It is currently running the automated tests.

### Dependencies

- Flask (for the web app)
- Psycopg2 (for accessing postgres)
- Pillow (for generating thumbnails)
- Pika (for integrating with RabbitMQ)
- Pytest (for testing)

All the dependencies of the project are managed with `pip`. During development `pipenv` was used to simplify dependency management.

### API

There is a Postman collection file in the root folder of this project containing the API requests, `photo-processing-postman.json`

The API consists of two endpoints.

- GET /photos/pending
It returns all the photos with `pending` status

- POST /photos/process
It is used to request the generation of thumbnails for a set of photos. Its request body is a JSON with the following format:

```
{
    "to_process": ["photo1_uuid", "photo2_uuid", ...]
}
```

### Further improvement

- Connection pool for DB
- Stop opening new connections with RabbitMQ every time it is needed
- Configure a dead letter for RabbitMQ and retries with a reasonable delay between them for every failing message in order to reduce the need for manual inspection
- Add retries for image download
- Solve the situation where a race condition happens when there is more than one consumer: if there is more than one register on the queue for the same photo, the two consumers could retrieve it and process it independently because the status on the DB would still be `processing`
- Integration tests
- Possibly improve mocking and tooling used for tests
- Possibly improve separation of Python modules
