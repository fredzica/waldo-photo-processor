#!/usr/bin/env python

"""

This is a standalone Python program that consumes a queue
of photos in order to create thumbnails for them. It also
manages the status of the processing by communicating with
a DB server.

The maximum size of the thumbnails is 320x320. They will be saved
in /waldo-app-thumbs folder with the ID of the photos as the file name.

"""

import urllib.request
import logging
import os
import uuid

import pika
import psycopg2

from psycopg2.extras import RealDictCursor
from PIL import Image

class UnexistentPendingPhotoError(Exception):
    pass

# setting up logging
logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger()


def db_connection():
    return psycopg2.connect(
        os.environ.get('PG_CONNECTION_URI', None),
        cursor_factory=RealDictCursor
    )


def fetch_pending_photo(photo_id, cursor):
    cursor.execute("SELECT * FROM photos WHERE uuid = %s AND status = 'pending'", (photo_id,))
    return cursor.fetchone()


def update_photo_status(photo_id, new_status, cursor):
    cursor.execute("UPDATE photos SET status = %s WHERE uuid = %s", (new_status, photo_id))


def save_thumbnail(photo_uuid, width, height, url, cursor):
    cursor.execute(
        "INSERT INTO photo_thumbnails(photo_uuid, width, height, url) "
        "VALUES(%s, %s, %s, %s)",
        (photo_uuid, width, height, url)
    )


def resize_image(photo_path, thumb_path):
    size = (320, 320)

    im = Image.open(photo_path)
    im.thumbnail(size)
    im.save(thumb_path, 'JPEG')

    return im


def create_thumbnail(photo):
    # updating photo status
    photo_id = photo['uuid']

    url = photo['url']
    photo_path = '/tmp/%s' % url.split('/')[-1]
    thumb_path = '/waldo-app-thumbs/%s.jpeg' % photo_id

    try:
        # download photo
        urllib.request.urlretrieve(url, photo_path)

        # generating thumbnail
        thumb = resize_image(photo_path, thumb_path)
    finally:
        # ensures temporary file is always deleted
        if os.path.isfile(photo_path):
            os.remove(photo_path)

    thumb.path = thumb_path
    return thumb


def process_photo(photo_id, cursor):
    try:
        photo = fetch_pending_photo(photo_id, cursor)

        # validates if exists in DB
        if photo is None:
            raise UnexistentPendingPhotoError("Id %s is not of a pending photo."
                                              % photo_id)

        update_photo_status(photo_id, 'processing', cursor)

        thumb = create_thumbnail(photo)
        save_thumbnail(photo_id, thumb.width, thumb.height, thumb.path, cursor)

        update_photo_status(photo_id, 'completed', cursor)

    except Exception as e:
        if photo is not None:
            update_photo_status(photo_id, 'failed', cursor)

        raise e


def callback(ch, method, properties, body):
    LOGGER.info("Received message %s", body)

    # validate UUID format
    photo_id = body.decode('utf-8')
    try:
        uuid.UUID(photo_id)
    except ValueError:
        LOGGER.exception('The received message from the queue is not a valid UUID')
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
        return

    # the actual processing of the message
    with db_connection() as db:
        with db.cursor() as cursor:
            try:
                process_photo(photo_id, cursor)
            except Exception:
                # the except must be broad because the consumer shoud not crash on errors
                LOGGER.exception("An error occured while processing message %s", body)

                ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)
                return


    ch.basic_ack(delivery_tag=method.delivery_tag)
    LOGGER.info("Done processing message with body %s", body)


def main():
    connection = pika.BlockingConnection(
        pika.URLParameters(os.environ.get('AMQP_URI', None)))

    channel = connection.channel()
    channel.queue_declare(queue='photo_processor', durable=True)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='photo_processor', on_message_callback=callback)
    channel.start_consuming()


if __name__ == '__main__':
    main()
