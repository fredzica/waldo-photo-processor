"""

This is a flask app that exposes a REST API to handle
the creation of thumbnails of photos.

It interacts with a DB server and a MQ server.

"""

import os
import logging

import psycopg2
import psycopg2.errors
from psycopg2.extras import RealDictCursor

import pika

import werkzeug.exceptions
from flask import Flask, g, request, jsonify

app = Flask(__name__)

def close_db(e=None):
    """Closes a db connection when its corresponding request ends its lifecycle"""

    db = g.pop('db', None)
    if db is not None:
        db.close()

# register the function to close db connections
app.teardown_appcontext(close_db)


def api_return(msg, status_code=500):
    resp = jsonify({'msg': msg})
    resp.status_code = status_code
    return resp


def db_conn():
    """Ensures that the returned db connection is tied to a request object"""

    # the 'g' object is present in each request
    if 'db' not in g:
        g.db = psycopg2.connect(
            os.environ.get('PG_CONNECTION_URI', None),
            cursor_factory=RealDictCursor
        )
    return g.db


def fetch_all_pending_photos():
    with db_conn() as connection:
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT uuid, url, created_at FROM photos "
                "WHERE status = 'pending' "
                "ORDER BY created_at")
            return cursor.fetchall()

def exists_pending_photos(uuids):
    with db_conn() as connection:
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT count(*) = %s AS exists FROM photos "
                "WHERE status = 'pending' AND uuid IN %s",
                (len(uuids), tuple(uuids))
            )
            return cursor.fetchone().get('exists', None)

def send_messages(messages):
    """ sends multiple messages to the queue """

    connection = pika.BlockingConnection(
        pika.URLParameters(os.environ.get('AMQP_URI', None)))

    channel = connection.channel()
    # ensuring the queue exists
    channel.queue_declare(queue='photo_processor', durable=True)

    # inserts every message into the queue
    for message in messages:
        channel.basic_publish(
            exchange='',
            routing_key='photo_processor',
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2,  # make message persistent
            ))

    connection.close()

@app.route('/photos/pending')
def photos_pending():
    return jsonify(fetch_all_pending_photos())

@app.route('/photos/process', methods=('POST',))
def photos_process():
    # validating input
    req_json = request.get_json()

    uuids = req_json.get('to_process', None)
    if not isinstance(uuids, list) or not uuids:
        return api_return("The input format is not correct. "
                          "Make sure you have informed the 'to_process' array", 400)

    try:
        if not exists_pending_photos(uuids):
            return api_return("At least one of the IDs was not found as a pending photo", 422)
    except psycopg2.errors.InvalidTextRepresentation:
        return api_return("You have informed an UUID with invalid format", 400)

    # inserting in the queue
    send_messages(uuids)

    return api_return("The informed photos are enqueued to be processed", 202)


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_json_syntax_error(error):
    logging.error(error)
    return api_return("The body of the request is not valid JSON", 400)


# global error handler to ensure a JSON response and do proper logging
@app.errorhandler(Exception)
def handle_unexpected_error(error):
    logging.error(error)
    return api_return("An unexpected error occured. Please contact the API owner")
