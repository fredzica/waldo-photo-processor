from unittest import mock
from unittest.mock import Mock
import pytest
import psycopg2.errors

from src import api

@pytest.fixture(scope='module')
def test_client():
    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = api.app.test_client()

    # Establish an application context before running the tests.
    ctx = api.app.app_context()
    ctx.push()

    yield testing_client  # this is where the testing happens!

    ctx.pop()


def test_photos_pending(monkeypatch, test_client):
    mocked = Mock()
    mocked.return_value = [{'uuid': '1234'}, {'uuid': '0e4824e7-2b22-4746-be65-95f059cc21b2'}]
    monkeypatch.setattr(api, 'fetch_all_pending_photos', mocked)

    response = test_client.get('/photos/pending')
    assert response.status_code == 200
    assert b'"uuid":' in response.data
    assert b'"0e4824e7-2b22-4746-be65-95f059cc21b2"' in response.data


def test_photos_pending_db_error(monkeypatch, test_client):
    mocked = Mock()
    mocked.side_effect = Exception()
    monkeypatch.setattr(api, 'fetch_all_pending_photos', mocked)

    response = test_client.get('/photos/pending')
    assert response.status_code == 500
    assert b'msg' in response.data
    assert b'unexpected error' in response.data


def test_photos_process_empty_json(test_client):
    response = test_client.post('/photos/process', json={})

    assert response.status_code == 400
    assert b'to_process' in response.data
    assert b'input format is not correct' in response.data


def test_photos_process_empty_array(test_client):
    response = test_client.post('/photos/process', json={'to_process': []})

    assert 400 == response.status_code
    assert b'to_process' in response.data
    assert b'input format is not correct' in response.data


def test_photos_process_unexistent_photo(monkeypatch, test_client):
    mocked = Mock()
    mocked.return_value = False
    monkeypatch.setattr(api, 'exists_pending_photos', mocked)

    data = {'to_process': ['0e4824e7-2b22-4746-be65-95f059cc21b2']}
    response = test_client.post('/photos/process', json=data)

    assert response.status_code == 422
    assert b'one of the IDs' in response.data
    assert b'not found' in response.data


def test_photos_process_invalid_uuid(monkeypatch, test_client):
    mocked = Mock()
    mocked.side_effect = psycopg2.errors.InvalidTextRepresentation()
    monkeypatch.setattr(api, 'exists_pending_photos', mocked)

    data = {'to_process': ['0e4824e7-2b!!!!!11111-95f059cc21b2']}
    response = test_client.post('/photos/process', json=data)

    assert response.status_code == 400
    assert b'UUID' in response.data
    assert b'invalid format' in response.data


def test_photos_process(monkeypatch, test_client):
    mocked = Mock()
    mocked.return_value = True
    monkeypatch.setattr(api, 'exists_pending_photos', mocked)
    monkeypatch.setattr(api, 'send_messages', Mock())

    data = {'to_process': ['0e4824e7-2b22-4746-be65-95f059cc21b2',
                           '5c4824e7-2b22-4746-be65-95f059cc21e5']}
    response = test_client.post('/photos/process', json=data)

    assert response.status_code == 202
    assert b'photos' in response.data
    assert b'to be processed' in response.data


@mock.patch('pika.BlockingConnection')
def test_send_messages(mocked_BlockingConnection):
    channel = Mock()
    mocked_BlockingConnection.return_value.channel.return_value = channel

    messages = ['a good message', 'a nice message', 'a fine message']
    api.send_messages(messages)

    assert channel.basic_publish.call_count == len(messages)
