"""
This file contains the unit tests for the photo_consumer queue consumer.
"""
from unittest.mock import Mock, MagicMock
from unittest import mock
import pytest

import src.photo_processor as processor

# testing with using a custom exception is more assertive than using Exception
class ExceptionMock(Exception):
    pass

def image_mock():
    im_mock = Mock()
    im_mock.size = (320, 192)
    im_mock.thumbnail = Mock()
    im_mock.save = Mock()

    return im_mock


@mock.patch('PIL.Image.open', return_value=image_mock())
def test_resize_image(im_mock):
    im = processor.resize_image('photo_path', 'thumb_path')
    im.thumbnail.assert_called_once_with((320, 320))
    im.save.assert_called_once_with('thumb_path', 'JPEG')
    assert im.size == (320, 192)


def test_create_thumbnail_no_url_error():
    photo = {'uuid' : 'test'}
    with pytest.raises(KeyError):
        processor.create_thumbnail(photo)


@mock.patch('os.remove')
@mock.patch('os.path.isfile')
@mock.patch('urllib.request.urlretrieve')
def test_create_thumbnail_downloading_photo_error(mocked_urlretrieve, mocked_isfile,
                                                  mocked_remove):
    photo = {
        'uuid': 'test',
        'url': 'broken/url3',
    }

    mocked_urlretrieve.side_effect = ExceptionMock()
    with pytest.raises(ExceptionMock):
        processor.create_thumbnail(photo)

    # temporary file was deleted even on error
    mocked_urlretrieve.assert_called_once_with('broken/url3', '/tmp/url3')
    mocked_isfile.assert_called_once_with('/tmp/url3')


@mock.patch('PIL.Image.open', return_value=image_mock())
@mock.patch('os.remove')
@mock.patch('os.path.isfile')
@mock.patch('urllib.request.urlretrieve')
def test_create_thumbnail(mocked_urlretrieve, mocked_isfile, mocked_remove, mocked_open):
    photo = {
        'uuid': 'test',
        'url': 'good/url56',
    }

    thumb = processor.create_thumbnail(photo)
    assert thumb.path == '/waldo-app-thumbs/test.jpeg'
    assert thumb.size == (320, 192)

    mocked_urlretrieve.assert_called_once_with('good/url56', '/tmp/url56')
    mocked_isfile.assert_called_once_with('/tmp/url56')


def test_process_photo_unexistent_photo(monkeypatch):
    mocked_fetch = Mock()
    mocked_fetch.return_value = None
    monkeypatch.setattr(processor, 'fetch_pending_photo', mocked_fetch)

    with pytest.raises(processor.UnexistentPendingPhotoError):
        processor.process_photo('test', None)


def test_process_photo_db_error(monkeypatch):
    photo = {
        'uuid': 'test',
    }

    mocked_fetch = Mock()
    mocked_fetch.return_value = photo
    monkeypatch.setattr(processor, 'fetch_pending_photo', mocked_fetch)

    mocked_update = Mock()
    mocked_update.side_effect = ExceptionMock()
    monkeypatch.setattr(processor, 'update_photo_status', mocked_update)

    with pytest.raises(ExceptionMock):
        processor.process_photo('test', None)


def test_process_photo(monkeypatch):
    photo = {
        'uuid': 'test',
    }

    mocked_fetch = Mock()
    mocked_fetch.return_value = photo
    monkeypatch.setattr(processor, 'fetch_pending_photo', mocked_fetch)

    mocked_update = Mock()
    monkeypatch.setattr(processor, 'update_photo_status', mocked_update)

    mocked_save = Mock()
    monkeypatch.setattr(processor, 'save_thumbnail', mocked_save)

    mocked_create_thumb = Mock()
    mocked_thumb = image_mock()
    mocked_create_thumb.return_value = mocked_thumb
    monkeypatch.setattr(processor, 'create_thumbnail', mocked_create_thumb)

    processor.process_photo('test', None)
    mocked_save.assert_called_once_with('test', mocked_thumb.width, mocked_thumb.height,
                                        mocked_thumb.path, None)


def test_callback(monkeypatch):
    mocked_process = Mock()
    monkeypatch.setattr(processor, 'process_photo', mocked_process)

    mocked_connection = MagicMock()
    monkeypatch.setattr(processor, 'db_connection', mocked_connection)

    ch = Mock()

    method = Mock()
    method.delivery_tag = 'some_tag'

    message = b'0e4824e7-2b22-4746-be65-95f059cc21b2'

    processor.callback(ch, method, {}, message)
    ch.basic_ack.assert_called_once()


def test_callback_invalid_uuid():
    ch = Mock()

    method = Mock()
    method.delivery_tag = 'some_tag'

    message = b'!!!!!!!!!!!!'

    processor.callback(ch, method, {}, message)
    ch.basic_reject.assert_called_once()


def test_callback_process_photo_error(monkeypatch):
    mocked_process = Mock()
    mocked_process.side_effect = ExceptionMock()
    monkeypatch.setattr(processor, 'process_photo', mocked_process)

    mocked_connection = MagicMock()
    monkeypatch.setattr(processor, 'db_connection', mocked_connection)

    ch = Mock()

    method = Mock()
    method.delivery_tag = 'some_tag'

    message = b'0e4824e7-2b22-4746-be65-95f059cc21b2'

    processor.callback(ch, method, {}, message)
    ch.basic_reject.assert_called_once()
